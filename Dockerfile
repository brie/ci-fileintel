FROM python:3.8-slim-buster

WORKDIR /app
RUN apt update
ENV DEBIAN_FRONTEND=noninteractive
RUN apt install -y git p7zip
RUN dpkg -L p7zip
RUN which p7zip

#COPY requirements.txt requirements.txt
#RUN ls -ahls /app/requirements.txt
#RUN pip3 install -r requirements.txt
RUN git clone https://github.com/keithjjones/fileintel.git
RUN pip3 install -r fileintel/requirements.txt

#COPY .  .
COPY config.conf hey.sh  hashes.txt /app/

RUN ls -ahls /app/config.conf
RUN ls -ahls /app/hashes.txt


#CMD [ "python3", "run.py"]
#CMD ["python3", "/app/fileintel/fileintel.py","config.conf","hashes.txt","-n"]
CMD ["sh","/app/hey.sh"]


# $ python fileintel.py myconfigfile.conf myhashes.txt -a > myoutput.csv



# pip install git+https://github.com/f5devcentral/f5-waf-tester.git



#FROM python:3.8-slim-buster
